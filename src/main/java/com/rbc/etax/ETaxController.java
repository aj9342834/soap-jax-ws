/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rbc.etax;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Andrew
 */
@Path("/documents")
public class ETaxController {
    
        /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
//    @GET
//    @Produces(MediaType.APPLICATION_XML)
//    public String search1() {
//        return "Love you Renee!";
//    }
    
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> search() {
        List<String> names = new ArrayList<String>();
        names.add("Andrew");
        names.add("Renee");
        return names;
    }
}
